package test.entities;

import org.junit.jupiter.api.Test;
import test.factory.AccountFactory;

import static org.junit.jupiter.api.Assertions.*;

public class AccountTests {

  @Test
  public void depositShouldIncreaseBalanceWhenPositiveAmount() {
	double amount = 200.00;
	var account = AccountFactory.createEmptyAccount();
	account.deposit(amount);
	double expectedValue = 196.0;

	assertEquals(expectedValue, account.getBalance());
  }

  @Test
  public void depositShouldDoNothingWhenNegativeAmount() {
	double initialBalance = 100.0;
	var account = AccountFactory.createAccount(initialBalance);
	double amount = -200.00;
	account.deposit(amount);
	double expectedValue = initialBalance;

	assertEquals(expectedValue, account.getBalance());
  }

  @Test
  public void withdrawShouldDecreaseBalanceWhenSufficientBalance() {
	var account = AccountFactory.createAccount(800.0);
	account.withdraw(500.0);
	assertEquals(300.0, account.getBalance());
  }

  @Test
  public void withdrawShouldThrowExceptionWhenInsufficientBalance() {
	assertThrows(IllegalArgumentException.class, () -> {
	  var account = AccountFactory.createAccount(800.0);
	  account.withdraw(801.0);
	});
  }

  @Test
  public void fullWithdrawShouldClearBalance() {
	double expectedValue = 0.0;
	double initialBalance = 800.0;
	var account = AccountFactory.createAccount(initialBalance);
	var result = account.fullWithdraw();

	assertEquals(expectedValue, account.getBalance());
	assertTrue(result == initialBalance);
  }

}
